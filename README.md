# Chess - Cpp
This is the backend to chess game application written in Cpp.

# Getting started

1. Clone the repo:
```
git clone https://gitlab.com/YuvalBrav/chess-by-guy-and-yuval-the-awesome-dudes-2021-wow
```
2. After cloning this git you need to run:
```
ChessGraphics.exe
```
3. Lastly - run the code 
```
Source.cpp
```

# Introduction
We reccomend this : https://www.chess.com/learn-how-to-play-chess site if you don't know how to play.
This game is made for 2 players that play on the same computer.


# Contact
Yuval Braverman - yuvalbr10@gmail.com
