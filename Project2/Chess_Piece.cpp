#include "ChessPiece.h"
#include "math.h"

// ctor
Chess_Piece::Chess_Piece(const char* name, const char* p)
{
	this->_name = new char[2];
	this->_placement = new char[3];
	memcpy(this->_name, name, 2);
	memcpy(this->_placement, p, 3);
}

//dtor
Chess_Piece::~Chess_Piece()
{
	delete[] this->_name;
	delete[] this->_placement;
}

//getters
char* Chess_Piece::getName() const
{
	return this->_name;
}
char* Chess_Piece::getPlacement() const
{
	return this->_placement;
}

//setter
void Chess_Piece::setPlacement(const char* move)
{
	this->_placement[0] = move[2];
	this->_placement[1] = move[3];
}
