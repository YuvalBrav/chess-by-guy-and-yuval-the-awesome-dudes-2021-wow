#pragma once
#include "ChessPiece.h"

class Knight : public Chess_Piece
{
public:
	//ctor dtor
	Knight(const char* name, const char* p);
	virtual ~Knight();
	//check for valid move
	virtual bool validMove(const char* move);
};

