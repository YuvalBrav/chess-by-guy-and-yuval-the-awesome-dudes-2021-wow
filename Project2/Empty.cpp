#include "Empty.h"

// ctor 
Empty::Empty(const char* p):
	Chess_Piece("#", p)
{
}

// dtor
Empty::~Empty()
{
}

// Function will always return false because there is no valid move for empty piece 
bool Empty::validMove(const char* move)
{
	return false;
}
