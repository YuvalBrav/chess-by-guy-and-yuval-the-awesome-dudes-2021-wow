#include "Pawn.h"

//ctor
Pawn::Pawn(const char* name, const char* p):
	Chess_Piece(name, p), _first_move(p[1] == '2' || p[1] == '7')
{
}

// dtor
Pawn::~Pawn()
{
}

/*
	Function checking if the move is valid
	Checks basic move only! (+ 1 / + 2)
*/
bool Pawn::validMove(const char* move)
{
	bool valid = false;//valid flag
	//check if valid
	if (this->getName()[0] == 'p') // for white pawns
	{
		if ((move[1] + 1 == move[3] && move[0] == move[2]) ||
			(move[1] + 2 == move[3] && move[0] == move[2] && this->_first_move)||
			(move[1] + 1 == move[3] && move[0] + 1 == move[2]) || //pawn eat
			(move[1] + 1 == move[3] && move[0] - 1 == move[2]) )
		{
			valid = true;
		}
	}
	else //for black pawns
	{
		if ((move[1] - 1 == move[3] && move[0] == move[2]) ||
			(move[1] - 2 == move[3] && move[0] == move[2] && this->_first_move)||
			(move[1] - 1 == move[3] && move[0] + 1 == move[2]) || //pawn eat
			(move[1] - 1 == move[3] && move[0] - 1 == move[2]) )
		{
			valid = true;
		}
	}

	return valid;
}
