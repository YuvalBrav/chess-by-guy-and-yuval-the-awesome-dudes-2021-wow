#include "Queen.h"

//ctor
Queen::Queen(const char* name, const char* p) :
    Chess_Piece(name, p)
{
}

//dtor
Queen::~Queen()
{
}

// Function checking if the move is valid
bool Queen::validMove(const char* move) 
{
    bool valid = true;//valid flag
    if ((move[1] != move[3] && move[0] != move[2]) &&
        (abs(move[0] - move[2]) != abs(move[1] - move[3])))//check if valid
    {
        valid = false;
    }
    return valid;
}
