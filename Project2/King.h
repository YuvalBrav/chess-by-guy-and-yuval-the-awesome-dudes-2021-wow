#pragma once

#include "ChessPiece.h"

class King : public Chess_Piece
{
public:
    //ctor dtor
	King(const char* name, const char* p);
	virtual ~King();
	//check for valid move
	virtual bool validMove(const char* move);
};
