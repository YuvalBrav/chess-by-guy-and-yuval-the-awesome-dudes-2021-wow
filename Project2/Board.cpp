#include "Board.h"

//ctor:
Board::Board(const char* board)
{
	int i = 0;//for loop
	for (i = 0; i < STR_BOARD_LEN; i++)
	{
		this->_str_board[i] = board[i];
	}
	//create board
	this->str_to_board();
}

//Function will create from str board 8*8 board.
void Board::str_to_board()
{
	int i = 0;//for loop
	int g = 0;//for loop
	char name[] = "#";//name of piece
	char p[] = "a1";//place of piece
	for (i = 0; i < BOARD_LEN; i++) //row - 123 
	{
		for (g = 0; g < BOARD_LEN; g++)//col - abc
		{
			name[0] = this->_str_board[i * BOARD_LEN + g];
			p[0] = 'a' + g;//the letter  
			p[1] = '1' + i;//the number
			//check and put each piece
			if (name[0] == 'p' || name[0] == 'P')
			{
				this->_board[i][g] = new Pawn(name, p);
			}
			else if (name[0] == 'b' || name[0] == 'B')
			{
				this->_board[i][g] = new Bishop(name, p);
			}
			else if (name[0] == 'n' || name[0] == 'N')
			{
				this->_board[i][g] = new Knight(name, p);
			}
			else if (name[0] == 'r' || name[0] == 'R')
			{
				this->_board[i][g] = new Rook(name, p);
			}
			else if (name[0] == 'q' || name[0] == 'Q')
			{
				this->_board[i][g] = new Queen(name, p);
			}
			else if (name[0] == 'k' || name[0] == 'K')
			{
				this->_board[i][g] = new King(name, p);
			}
			else if (name[0] == '#')
			{
				this->_board[i][g] = new Empty(p);
			}
		}
	}
}

//Function will delete the board
void Board::delete_board()
{
	int i = 0;//for loop
	int g = 0;//for loop
	for (i = 0; i < BOARD_LEN; i++)
	{
		for (g = 0; g < BOARD_LEN; g++)
		{
			delete this->_board[i][g];
		}
	}
}

//Function will check if the way of the Chess piece is empty (no other chess pieces in the middle)
bool Board::empty_way(Chess_Piece* p, const char* move) const
{
	char tempP[] = "00";//temp placement
	const char* name = p->getName();//name of piece  
	int i = 0;//for loop
	int wayLen = 0;//length of way to move
	int moveIndex = 0;//which axis to move on
	int forwardNum = 0;//forward- 1 backward- -1 for num
	int forwardChar = 0;//forward- 1 backward- -1 for char
	bool emptyway = true;//flag if empty way
	memcpy(tempP, p->getPlacement(), 3);
	if (tolower(name[0]) == 'r' ||
		tolower(name[0]) == 'p' ||
		tolower(name[0]) == 'k' ||
		tolower(name[0]) == 'q') //moves for queen, king, pawn and rook
	{
		if (move[0] == move[2]) // if the chess piece want to move left -> right or right -> left
		{
			wayLen = abs(move[1] - move[3]);
			forwardNum = (move[3] - move[1]) / abs(move[1] - move[3]);
			moveIndex = 1;
			if (tolower(name[0]) == 'p')
			{
				wayLen++;
			}
		}
		else if (move[1] == move[3]) // if the chess piece want to move up -> down or down -> up
		{
			wayLen = abs(move[0] - move[2]);
			forwardNum = (move[2] - move[0]) / abs(move[0] - move[2]);
			moveIndex = 0;
		}
		if (wayLen != 0) 
		{
			tempP[moveIndex] = move[moveIndex] + forwardNum;
			for (i = 1; i < wayLen; i++)
			{
				if (find_chess_piece(tempP)->getName()[0] != '#')
				{
					emptyway = false;
				}
				tempP[moveIndex] = tempP[moveIndex] + forwardNum;
			}
		}
	}
	if (tolower(name[0]) == 'b' ||
		(tolower(name[0]) == 'q' && wayLen == 0))  //moves for queen(if it havent moved up/down/left/right), bishop
	{
		wayLen = abs(move[1] - move[3]);
		forwardNum = (move[3] - move[1]) / abs(move[1] - move[3]);
		forwardChar = (move[2] - move[0]) / abs(move[0] - move[2]);
		tempP[1] = move[1] + forwardNum;
		tempP[0] = move[0] + forwardChar;
		for (i = 1; i < wayLen; i++)
		{
			if (find_chess_piece(tempP)->getName()[0] != '#') 
			{
				emptyway = false;
			}
			tempP[1] = tempP[1] + forwardNum;
			tempP[0] = tempP[0] + forwardNum;
		}
	}
	if(tolower(name[0]) == 'p' && wayLen == 0) // eat move on pawn
	{
		tempP[0] = move[2];
		tempP[1] = move[3];
		wayLen = abs(move[1] - move[3]); // have to be 1 
		if (find_chess_piece(tempP)->getName()[0] == '#' || wayLen != 1)
		{
			emptyway = false;
		}
	}
	if (emptyway) // if its true then set the new placement
	{
		p->setPlacement(move);
	}
	return emptyway;
}

//dtor:
Board::~Board()
{
	int i = 0;
	this->delete_board();
	delete[] this->_str_board;
}

//returning chess piece by placement:
Chess_Piece* Board::find_chess_piece(const char* p) const
{
	return this->_board[p[1] - '1'][p[0] - 'a'];
}

/*
Checking if the board is valid after the move:
	1. Check this index (ERROR 7: Move start and target at the same point
							ERROR 5: Index is invalid)
	2. Check the chess pieces (ERROR 2: No chess piece in the require start(of the curr player) 
								ERROR 3: In the target point there is chess piece (of the curr player))
	3. Check valid move (ERROR 6: Invalid move)
	4. Check Check on the curr player (ERROR 4: Invalid move) 
	5. IF EVERYTHING FINE -> CODE 0 
	6. Check if there is check on the other player (CODE 1) -> Checkmate? (CODE 8)
*/
unsigned int Board::valid_board(const char* move)
{
	unsigned int code = 0;//code for move
	char tempBoardStr[STR_BOARD_LEN] = { 0 };//temperary str of board
	char start[] = "00";//start piece place
	char target[] = "00";//target piece place
	int turn = this->_str_board[64] - '0'; // 0 -> white + lowercase, 1 -> black + uppercase
	char* tempBoard = nullptr;//temperary board

	Chess_Piece* startPiece = nullptr;//start piece
	Chess_Piece* targetPiece = nullptr;//target piece

	// find start piece
	start[0] = move[0];
	start[1] = move[1];
	startPiece = find_chess_piece(start);

	// find targrt piece
	target[0] = move[2];
	target[1] = move[3];
	targetPiece = find_chess_piece(target);

	// 1: Checking index
	if (!(move[0] >= 'a' && move[0] <= 'h' &&
		move[2] >= 'a' && move[2] <= 'h' &&
		move[1] >= '1' && move[1] <= '8' &&
		move[3] >= '1' && move[3] <= '8'))	//	if not in range
	{
		code = 5;
	}
	else if (move[0] == move[2] && move[1] == move[3])	// staying on the same place
	{
		code = 7;
	}
	else if (this->turn_by_piece(startPiece) != turn) // if trying to play with not your piece 
	{
		code = 2;
	}
	else if (this->turn_by_piece(targetPiece) == turn)// if trying to eat your piece
	{
		code = 3;
	}
	else if (!startPiece->validMove(move) || !this->empty_way(startPiece, move))// check if valid move and empty way
	{
		code = 6;
	}

	// valid moves:
	memcpy(tempBoardStr, this->_str_board, STR_BOARD_LEN);// save the board
	if (code == 0)
	{
		this->_str_board[64] = !turn + '0';//chaning the turn
		this->_str_board[((move[3] - '0') - 1) * BOARD_LEN + (move[2] - 'a')] =
			this->_str_board[((move[1] - '0') - 1) * BOARD_LEN + (move[0] - 'a')];//making the move
		this->_str_board[((move[1] - '0') - 1) * BOARD_LEN + (move[0] - 'a')] = '#'; //empty space
		this->delete_board(); //deleting the board
		this->str_to_board(); //making the new move
		if (is_check((King*)find_piece_by_name(turn ? "K" : "k")))//if check on curr player
		{
			code = 4;
			memcpy(this->_str_board, tempBoardStr, STR_BOARD_LEN); //returning the last step because its invalid move
		}
		else if (is_check((King*)find_piece_by_name(turn ? "k" : "K")))//if check on other player
		{
			code = 1;
		}
		else if(!find_piece_by_name(turn ? "k" : "K"))//if king is dead
		{
			code = 8;
		}
	}
	this->delete_board(); // creating the new move / the old one.
	this->str_to_board();
	return code;
}

// Returning board str
char* Board::get_board() const
{
	char str_board[STR_BOARD_LEN];
	memcpy(str_board, this->_str_board, STR_BOARD_LEN);
	return str_board;
}

// Function getting piece and checking who is playing now(by the name)
// 1 - black, 0 - white, -1 - empty(no one is playing)
int Board::turn_by_piece(const Chess_Piece* p) const
{
	char name = p->getName()[0];//name of piece
	int turn = 1;//flag for turn (black)
	if (name >= 'a' && name <= 'z')//if white
	{
		turn = 0;
	}
	else if (name == '#')//if empty slot
	{
		turn = -1;
	}
	return turn;
}

// Checking if check is currently on the passed king
bool Board::is_check(const King* k) const
{
	char move[] = "0000";//move on king
	Chess_Piece* currPiece = nullptr;//curr piece to check
	bool check = false;//flag if check is happend
	int i = 0; // for loop
	int g = 0; // for loop
	// getting the curr king placement
	move[2] = k->getPlacement()[0];
	move[3] = k->getPlacement()[1];
	//checking all pieces of opposite color from king
	for (i = 0; i < BOARD_LEN && !check; i++)
	{
		for (g = 0; g < BOARD_LEN && !check; g++)
		{
			currPiece = this->_board[i][g]; // checking every piece in board
			if (this->turn_by_piece(currPiece) != this->turn_by_piece(k)) // not same turn
			{
				move[0] = currPiece->getPlacement()[0]; // getting the curr placement of the curr piece
				move[1] = currPiece->getPlacement()[1];
				// if the curr piece can move to the king and the way to it is empty
				if (currPiece->validMove(move) && this->empty_way(currPiece,move))
				{
					check = true;
				}
			}
		}
	}
	return check;
}

// Function will find piece by its name
Chess_Piece* Board::find_piece_by_name(const char* name) const
{
	Chess_Piece* currPiece = nullptr;//curr piece for checking
	Chess_Piece* pieceByName = nullptr;//piece we are looking for
	int i = 0; // For loop
	int g = 0; // For loop
	for (i = 0; i < BOARD_LEN && !pieceByName; i++)
	{
		for (g = 0; g < BOARD_LEN && !pieceByName; g++)
		{
			currPiece = this->_board[i][g]; // going thought every piece on the board
			if (currPiece->getName()[0] == name[0]) // if the cuur piece name is equal to the passed name
			{
				pieceByName = currPiece;
			}
		}
	}
	return pieceByName;
}
