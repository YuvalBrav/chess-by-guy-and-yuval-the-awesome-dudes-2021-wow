#pragma once

#include "ChessPiece.h"

class Rook : public Chess_Piece
{
public:
    //ctor dtor
	Rook(const char* name, const char* p);
	virtual ~Rook();
	//check for valid move
	virtual bool validMove(const char* move);
};
