#pragma once
#include "ChessPiece.h"
class Empty : public Chess_Piece
{
public:
	//ctor dtor
	Empty(const char* p);
	virtual ~Empty();
	//check for valid move
	virtual bool validMove(const char* move);
};

