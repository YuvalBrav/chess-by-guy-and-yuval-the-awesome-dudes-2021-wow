#include "Knight.h"

// ctor
Knight::Knight(const char* name, const char* p):
	Chess_Piece(name, p)
{
}
// dtor
Knight::~Knight()
{
}

// Function checking if the move is valid
bool Knight::validMove(const char* move)
{
    bool valid = false;//valid flag
    //check if valid
    if (((move[0] + 2 == move[2] || move[0] - 2 == move[2]) && 
            (move[1] + 1 == move[3] || move[1] - 1 == move[3]))||
 
        ((move[0] + 1 == move[2] || move[0] - 1 == move[2]) && 
            (move[1] + 2 == move[3] || move[1] - 2 == move[3]))) 
    {
        valid = true;
    }
    return valid;
}
