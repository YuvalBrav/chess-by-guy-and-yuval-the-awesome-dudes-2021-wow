#pragma once

#include "ChessPiece.h"

class Bishop : public Chess_Piece
{
public:
	//ctor dtor
	Bishop(const char* name, const char* p);
	virtual ~Bishop();
	//check for valid move
	virtual bool validMove(const char* move);
};

