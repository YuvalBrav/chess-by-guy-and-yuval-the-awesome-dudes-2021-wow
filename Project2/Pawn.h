#pragma once

#include "ChessPiece.h"

class Pawn : public Chess_Piece
{
public:
	//ctor dtor
	Pawn(const char* name, const char* p);
	virtual ~Pawn();
	//check for valid move
	virtual bool validMove(const char* move);

private:
	bool _first_move;//if already done first move
};

