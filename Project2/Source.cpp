#include "Pipe.h"
#include "Board.h"
#include <iostream>
#include <thread>

using std::cout;
using std::endl;
using std::string;


void main()
{
	srand(time_t(NULL));

	
	Pipe p;
	bool isConnect = p.connect();
	
	string ans;
	//connecting to frontend
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return;
		}
	}
	

	char msgToGraphics[1024];
	char code[2] = { 0 };
	// msgToGraphics should contain the board string accord the protocol
	strcpy_s(msgToGraphics, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0");
	  
	// send the board string
	p.sendMessageToGraphics(msgToGraphics);

	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();

	// creating the board
	Board board(msgToGraphics); 
	while (msgFromGraphics != "quit")
	{
		//getting the code from valid_board
		code[0] = board.valid_board(msgFromGraphics.c_str()) + '0';

		// msgToGraphics should contain the result of the operation
		strcpy_s(msgToGraphics, code); 

		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);   

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}

	//closing the connection
	p.close();
}