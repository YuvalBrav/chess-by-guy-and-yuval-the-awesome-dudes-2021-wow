#pragma once

#include "ChessPiece.h"

class Queen :  public Chess_Piece
{
public:
    //ctor dtor
	Queen(const char* name, const char* p);
	virtual ~Queen();
	//check for valid move
	virtual bool validMove(const char* move);
};

