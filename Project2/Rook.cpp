#include "Rook.h"

// ctor
Rook::Rook(const char* name, const char* p): 
	Chess_Piece(name, p)
{
}

// dtor
Rook::~Rook()
{
}

// Function checking if the move is valid
bool Rook::validMove(const char* move)
{
//b4 d4
	bool valid = true;//valid flag
	if (!((move[1] == move[3] && move[0] != move[2]) ||
	(move[1] != move[3] && move[0] == move[2])))//check if valid
	{
		valid = false;
	}
	return valid;
}
