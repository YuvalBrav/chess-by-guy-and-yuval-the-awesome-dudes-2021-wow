#include "King.h"

//ctor
King::King(const char* name, const char* p) :
	Chess_Piece(name, p)
{
}

//dtor
King::~King()
{
}

// Function checking if the move is valid
bool King::validMove(const char* move)
{
	bool valid = false;//valid flag
	//check if valid
	if ((move[0] + 1 == move[2] && move[1] == move[3]) ||
		(move[0] - 1 == move[2] && move[1] == move[3]) ||
		(move[1] + 1 == move[3] && move[0] == move[2]) ||
		(move[1] - 1 == move[3] && move[0] == move[2]) ||
		(move[0] + 1 == move[2] && move[1] + 1 == move[3]) ||
		(move[0] - 1 == move[2] && move[1] - 1 == move[3]) ||
		(move[0] - 1 == move[2] && move[1] + 1 == move[3]) ||
		(move[0] + 1 == move[2] && move[1] - 1 == move[3]))
	{
		valid = true;
	}
	return valid;
}
