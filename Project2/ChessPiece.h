#pragma once

#include <iostream>

class Chess_Piece
{
public:
	//ctor dtor
	Chess_Piece(const char* name, const char* p);
	virtual ~Chess_Piece();
	//pure virtual
	virtual bool validMove(const char* move) = 0;
	//getters
	char* getName() const;
	char* getPlacement() const;
	//setters
	void setPlacement(const char* move);

private:
	char* _name;//name of piece

protected:
	char* _placement;//placment in the board
};