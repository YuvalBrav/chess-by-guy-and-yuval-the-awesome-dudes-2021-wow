#pragma once

#include "Bishop.h"
#include "Empty.h"
#include "King.h"
#include "Knight.h"
#include "Pawn.h"
#include "Queen.h"
#include "Rook.h"

#include <cctype> //for lowercase / uppercase

#define STR_BOARD_LEN 66
#define BOARD_LEN 8

class Board
{
public:
	//ctor dtor
	Board(const char* board);
	~Board();
	//methods
	Chess_Piece* find_chess_piece(const char* p) const;
	unsigned int valid_board(const char* move);
	char* get_board() const;
	int turn_by_piece(const Chess_Piece* p)const;

private:
	//method
	bool is_check(const King*) const;
	Chess_Piece* find_piece_by_name(const char* name) const;
	void str_to_board();
	void delete_board();
	bool empty_way(Chess_Piece* p, const char* move) const;
	//bool is_check_mate(const King*) const;
	
	//fields
	char _str_board[66];//str view of board
	Chess_Piece* _board[8][8];//board with pieces
};
