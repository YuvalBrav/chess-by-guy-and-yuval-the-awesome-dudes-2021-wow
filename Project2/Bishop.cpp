#include "Bishop.h"

// ctor:
Bishop::Bishop(const char* name, const char* p) :
    Chess_Piece(name, p)
{
}

//dtor:
Bishop::~Bishop()
{
}

// Function checking if the move is valid
bool Bishop::validMove(const char* move)
{
    bool valid = true;//valid flag
    //check if valid
    if ((abs(move[0] - move[2]) != abs(move[1] - move[3])))
    {
        valid = false;
    }
    return valid;
}